"use strict";

const dotenv = require('dotenv');
dotenv.config();

const url_base = process.env.URL + ":" + process.env.PORT;

let data;

const calculateDamage = (min, max) => {
  return Math.max(Math.floor(Math.random() * max) + 1, min);
};

const estVaincu = () => {
  if (data.monstreSante <= 0 || this.joueurSante <= 0) {
    return true;
  } 
  return false;
};

exports.gameInit = (req, res, next) => {
  data = {
    joueurSante: 100,
    monstreSante: 100,
  }
  res.json(data, [
    { 
      rel: "self",
      method: "GET",
      href: url_base + "/"
    },
    {
      rel: "attaque",
      method: "GET",
      href: url_base + "/attaque/"
    },
    {
      rel: "sante",
      method: "GET",
      href: url_base + "/sante/"
    }
  ]);
};

exports.attaque = (req, res, next) => {
  // Attaque du joueur sur le monstre
  let damage = calculateDamage(3, 10);
  data.monstreSante -= damage;

  // Attaque du monstre
  damage = calculateDamage(3, 10);
  data.joueurSante -= damage;

  // Teste si l'un des deux joueurs est vaincu !
  // Si oui, le jeu est terminé, on peut seulement
  // retourner vers / pour initialiser le jeu
  if (estVaincu()) {
    res.json(data, [
      { 
        rel: "self",
        method: "GET",
        href: url_base + "/attaque"
      },
      {
        rel: "init",
        method: "GET",
        href: url_base + "/"
      }
    ]);
  }

  res.json(data, [
    { 
      rel: "self",
      method: "GET",
      href: url_base + "/attaque"
    },
    {
      rel: "attaque",
      method: "GET",
      href: url_base + "/attaque/"
    },
    {
      rel: "sante",
      method: "GET",
      href: url_base + "/sante/"
    }
  ], [ data.joueurSante > 50 ? 'sante':'' ]);
  // N'affiche pas "sante" si la santé est > 50
};


exports.sante = (req, res, next) => {
  // On se refait une santé
  data.joueurSante += 10;

  // Attaque du monstre
  let damage = calculateDamage(3, 10);
  data.joueurSante -= damage;

  
  res.json(data, [
    { 
      rel: "self",
      method: "GET",
      href: url_base + "/sante"
    },
    {
      rel: "attaque",
      method: "GET",
      href: url_base + "/attaque/"
    }
    // N'affiche pas la santé car on ne peut pas
    // se refaire une santé deux fois de suite
    // {
    //   rel: "sante",
    //   method: "GET",
    //   href: url_base + "/sante/"
    // }
  ]);
};
